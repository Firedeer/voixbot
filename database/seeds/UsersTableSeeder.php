<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name'=>'juan',
                'email'=>'jhonny8522@gmail.com',
                'password'=>bcrypt('123456')
            ]
            );
            User::create(
                [
                    'name'=>'Maria',
                    'email'=>'mafe2@gmail.com',
                    'password'=>bcrypt('123456')
                ]
                );
                User::create(
                    [
                        'name'=>'johnny',
                        'email'=>'jhonny_sg10@hotmail.com',
                        'password'=>bcrypt('123456')
                    ]
                    );
    }
}
