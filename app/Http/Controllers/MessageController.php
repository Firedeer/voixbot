<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\messages;
use DB;
use Storage;
class MessageController extends Controller
{
    public function index(Request $request)
    {
        $userId=auth()->id();
    
        $contactId=$request->contact_id;

        if(isset($userId))
        {
        return messages::select(
        'id'
        ,DB::raw("IF(`from_id`= $userId,1,0) as receptor")
        ,DB::raw("IF(`from_id`= $userId,'info','light') as variant")
        ,'from_id'
        ,'to_id'
        ,'created_at'
        ,'content'
        )->where(function ($query) use($userId,$contactId) {
            $query->where('from_id',$userId)->where('to_id',$contactId);
            
        })->orwhere(function ($query) use($userId,$contactId) {

            $query->where('from_id',$contactId)->where('to_id',$userId);
        })->get();
        
        
        
        }
        return json_encode(array('message'=>"error:no hay ningun usuario autenticado"));
    }


    public function store(Request $request)
    {
        Storage::disk('local')->put('data.txt',json_encode($request));
        $message = new messages();
        $message->from_id = auth()->id();
        $message->to_id = $request->to_id;
        $message->content =  $request->content;

        $saved = $message->save();

        $data=[];
        $data['success'] = $saved;
        $data['message'] = $message;
        return $data;

    }
}
