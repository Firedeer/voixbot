<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\conversation;
class ConversationController extends Controller
{
    

    public function index()
    {

        return conversation::where('user_id',auth()->id())
        ->get([
            'id',
            'contact_id',
            'has_blocked',
            'listen_notifications',
            'last_message',
            'last_time'
        ]);
    }
}
