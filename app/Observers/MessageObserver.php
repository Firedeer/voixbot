<?php

namespace App\Observers;
use App\Events\MessageSent;
use App\messages;
use App\conversation;
class MessageObserver
{
    /**
     * Handle the Message "created" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function created(messages $message)
    {
        $conversation=conversation::where('user_id',$message->from_id)
                                    ->where('contact_id',$message->to_id)
                                    ->first();

        if($conversation)
        {
            $conversation->last_message="Tu: $message->content";
            $conversation->last_time=$message->created_at;
            $conversation->save();
        }


        $conversation=conversation::where('contact_id',$message->from_id)
        ->where('user_id',$message->to_id)
        ->first();

        if($conversation)
        {
            $conversation->last_message="$conversation->contact_name: $message->content";
            $conversation->last_time=$message->created_at;
            $conversation->save();
        }
        event(new MessageSent($message));
    }

}