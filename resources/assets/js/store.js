import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex);
export default  new Vuex.Store({
    state: {
      messages:[],
      selectedConversation:null,
      conversations:[],
      querySearch: '',
      user_id:null
    },
    mutations: {
      setUser(state,user)
      {
        state.user_id=user;
      },
      newMessageList (state,messages) {
        state.messages=messages;
      },
      newConversationList (state,conversations) {
        //console.log("conversacion newconversation",conversations);
        state.conversations=conversations;
      },
      addMessage(state,message)
      {
        const conversation=state.conversations.find((conversation)=>
        {
            return conversation.contact_id==message.from_id || 
                    conversation.contact_id==message.to_id
        }
        );
        const author= state.user_id===message.from_id ? 'Tú': conversation.contact_name
        conversation.last_message= `${author}:${message.content}`;
        conversation.last_time= message.created_at;
   
         if(state.selectConversation.contact_id==message.from_id ||state.selectConversation.contact_id==message.to_id )
         {
          state.messages.push(message);
         }
        
      },
      selectConversation(state,conversation)
      {
        state.selectedConversation=conversation;
      },
      newQuerySearch(state,newValue)
      {
        state.querySearch=newValue;
      }
    },
    actions:
    {
        getMessages(context,conversation )
        {
          
          axios.get(`/api/messages?contact_id=${conversation.contact_id}`)
          .then((response)=> {
              
           context.commit('newMessageList',response.data);
           context.commit('selectConversation',conversation);
        });

        },
        getConversation(context)
        {
            return axios.get('/api/conversations')
          .then((response)=> {  
           // console.log('getConversation',response.data);
            context.commit('newConversationList',response.data);            
         
          
        });
        },
        postMessages(context,newMessage)
        {
          const params = {
            to_id:context.state.selectConversation.contact_id,
            content:newMessage
          };
          axios.post('/api/messages',params)
        .then((response)=> {
              //console.log(response.data);
              if(response.data.success)
              {
              newMessage='';
              const message=response.data.message;
              message.receptor=true;
              message.variant="info";
              context.commit('addMessage',message);
              //this.getMessages();
               }
        });
        },

    }
    ,
    getters:
    {
      
        conversationsFiltered(state)
        {
         
          return state.conversations.filter(
            conversation=>
            conversation.contact_name.toLowerCase()
            .includes(state.querySearch.toLowerCase())
          );   
        },
        getconversationById(state)
        {
          return function(conversationId)
          {
            return state.conversations.find(
              conversation=>
                conversation.id==conversationId
            );
          }
         
        }
    }
  });