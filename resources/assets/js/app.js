
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import store from './store'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import messenger_Component from './components/MessengerComponent.vue'
import example_Component from './components/ExampleComponent.vue'
Vue.use(BootstrapVue);
Vue.use(VueRouter);





Vue.component('status_componente',require('./components/StatusContactComponent') );
//Vue.component('messenger_componente',require('./components/MessengerComponent.vue') );
Vue.component('contact_componente',require('./components/ContactComponent.vue') );
Vue.component('contactlist',require('./components/ContactListComponent.vue') );
Vue.component('conversationactiva',require('./components/ActiveConversationComponent.vue') );
Vue.component('messageconversation',require('./components/MessageConversationComponent.vue') );
Vue.component('searchcontactlist',require('./components/SearchContactListComponent.vue') );



const routes = [
  { path: '/chat', component: messenger_Component },
  { path: '/chat/:conversationId', component: messenger_Component },
  { path: '/example', component: example_Component }
]

const router = new VueRouter({
  routes,
  mode:'history'
});


const app = new Vue({
    el: '#app',
    store,
    router,
    methods: {
        logout() {
            document.getElementById('logout-form').submit();
        }
    }
});
